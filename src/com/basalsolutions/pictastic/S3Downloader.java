package com.basalsolutions.pictastic;

import java.util.ArrayList;

import com.amazonaws.services.s3.AmazonS3Client;

import android.content.Context;
import android.os.AsyncTask;

public class S3Downloader extends AsyncTask<Void, Void, ArrayList<PictasticImageView>> {
	private String _bucketName;
	private Context _context;
	private S3DownloadListener _listener;
	private AmazonS3Client _s3Client;

	public S3Downloader(Context context, AmazonS3Client s3client, String bucketname, S3DownloadListener listener) {
		_context = context;
		_listener = listener;
		_bucketName = bucketname;
		_s3Client = s3client;
	}

	
	@Override
	protected ArrayList<PictasticImageView> doInBackground(Void... params) {
		// Need to get the list of file from Amazon first
		return null;
	}

	@Override
	protected void onPostExecute(ArrayList<PictasticImageView> result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
	}

}
