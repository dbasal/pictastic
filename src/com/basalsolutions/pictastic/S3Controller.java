package com.basalsolutions.pictastic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.location.Location;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.google.common.io.Files;

public class S3Controller {
	private AmazonS3Client _s3Client;
	private static final String _bucketName = "pictastictestbucket";
	private Context _context;
	private S3UploadListener _s3UploadListener;
	private Messenger _s3SvcMessenger;
	private boolean _serviceBound = false;
	private Messenger _messenger = new Messenger(new IncomingHandler());
	private Intent _serviceIntent;
	private HashMap<String, Integer> _notiMap = new HashMap<String, Integer>();
	private HashMap<String, PicData> _dataMap = new HashMap<String, PicData>();
	private DBController _dbc;
	private int _totalNumFiles = 0;
	private int _processedFiles = 0;
	private List<PicData> _uploadedList = new ArrayList<PicData>();

	public void set_s3UploadListener(S3UploadListener listener) {
		this._s3UploadListener = listener;
	}

	public S3Controller(Context context, AWSCredentials creds) {
		_context = context;
		_s3Client = new AmazonS3Client(creds);
		_s3Client.setRegion(Region.getRegion(Regions.US_WEST_2));
		_serviceIntent = new Intent(_context, S3Service.class);
		/*
		 * ComponentName cn = _context.startService(_serviceIntent);
		 * 
		 * if (cn == null) { Log.i(this.getClass().getName(), "null returned"); }
		 */
		_dbc = new DBController(_context);

		// Bind the service
		bindService();
	}

	@SuppressLint("NewApi")
	private void createUploadCompleteNotification(int notifyid, String filename) {
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(_context).setSmallIcon(android.R.drawable.stat_sys_upload_done)
				.setContentTitle("Upload File Complete").setContentText(Files.getNameWithoutExtension(filename));

		// Creates an explicit intent for an Activity in your app
		Intent resultIntent = new Intent(_context, MainActivity.class);

		// The stack builder object will contain an artificial back stack for the
		// started Activity.
		// This ensures that navigating backward from the Activity leads out of
		// your application to the Home screen.
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(_context);

		// Adds the back stack for the Intent (but not the Intent itself)
		stackBuilder.addParentStack(MainActivity.class);

		// Adds the Intent that starts the Activity to the top of the stack
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
		mBuilder.setContentIntent(resultPendingIntent);
		NotificationManager mNotificationManager = (NotificationManager) _context.getSystemService(Context.NOTIFICATION_SERVICE);

		mNotificationManager.notify(notifyid, mBuilder.build());
	}

	@SuppressLint("NewApi")
	private int createUploadNotification(String filename) {
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(_context).setSmallIcon(android.R.drawable.stat_sys_upload)
				.setContentTitle("Uploading File").setContentText(Files.getNameWithoutExtension(filename));
		// Creates an explicit intent for an Activity in your app
		Intent resultIntent = new Intent(_context, MainActivity.class);

		// The stack builder object will contain an artificial back stack for the
		// started Activity.
		// This ensures that navigating backward from the Activity leads out of
		// your application to the Home screen.
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(_context);

		// Adds the back stack for the Intent (but not the Intent itself)
		stackBuilder.addParentStack(MainActivity.class);

		// Adds the Intent that starts the Activity to the top of the stack
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
		mBuilder.setContentIntent(resultPendingIntent);
		NotificationManager mNotificationManager = (NotificationManager) _context.getSystemService(Context.NOTIFICATION_SERVICE);

		Random rand = new Random();

		int id = rand.nextInt();
		mNotificationManager.notify(id, mBuilder.build());
		return id;
	}

	private void bindService() {
		Log.i(this.getClass().getName(), "+++++ bindService +++++");

		_context.getApplicationContext().bindService(_serviceIntent, s3ServiceConnection, Context.BIND_AUTO_CREATE);

		Log.i(this.getClass().getName(), "----- bindService -----");
	}

	private void unbindService() {
		if (_serviceBound) {
			if (_s3SvcMessenger != null) {
				try {
					Message msg = Message.obtain(null, Constants.MSG_UNREGISTER_CLIENT);
					msg.replyTo = _messenger;
					_s3SvcMessenger.send(msg);
				} catch (RemoteException e) {
					// There is nothing special we need to do if the service
					// has crashed.
				}
			}

			_context.unbindService(s3ServiceConnection);
			_serviceBound = false;
		}
	}

	public void uploadFiles(List<PicData> files) {
		// Just in case
		_dataMap.clear();
		_notiMap.clear();

		_totalNumFiles = files.size();

		for (PicData pd : files) {
			_dataMap.put(Files.getNameWithoutExtension(pd.get_imageLocalPath()), pd);

			S3Info s3info = new S3Info();
			s3info.set_bucket(_bucketName);
			s3info.set_s3Client(_s3Client);
			s3info.set_filename(pd.get_imageLocalPath());

			Message msg = Message.obtain(null, Constants.MSG_UPLOAD_FILE);

			// send a message to the service with the s3Info as an object.
			msg.obj = s3info;

			try {
				_s3SvcMessenger.send(msg);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	}

	/*
	 * @Override public void uploadComplete(List<PicData> list) { // This is where we should update the database with
	 * the new files // Then we can actually load them in the main app _s3UploadListener.uploadComplete(list); }
	 */
	// Make the connection to the service
	private ServiceConnection s3ServiceConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			Log.i(this.getClass().getName(), "+++++ onServiceConnected +++++");
			_s3SvcMessenger = new Messenger(service);
			try {
				Message msg = Message.obtain(null, Constants.MSG_REGISTER_CLIENT);
				msg.replyTo = _messenger;
				_s3SvcMessenger.send(msg);
				_serviceBound = true;
			} catch (RemoteException e) {
				// Here, the service has crashed even before we were able to
				// connect
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName className) {
			Log.i(this.getClass().getName(), "+++++ onServiceDisconnected +++++");
			_serviceBound = false;
		}
	};

	// Handle messages returned from the service
	class IncomingHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			Log.i(this.getClass().getName(), "+++++ handleMessage +++++");
			String filename = null;
			switch (msg.what) {
				case Constants.MSG_FILE_UPLOAD_START:
					Log.i(this.getClass().getName(), "MSG_FILE_UPLOAD_START");
					filename = (String) msg.obj;
					int id = createUploadNotification(filename);
					_notiMap.put(filename, new Integer(id));
					break;
				case Constants.MSG_FILE_UPLOAD_SUCCESS:
					Log.i(this.getClass().getName(), "MSG_FILE_UPLOAD_SUCCESS");

					// Notify the user the upload is complete
					filename = (String) msg.obj;
					int nid = _notiMap.get(filename);

					createUploadCompleteNotification(nid, filename);

					PicData pd = _dataMap.get(Files.getNameWithoutExtension(filename));

					// Let the folks listening know we have completed the upload of this file
					_s3UploadListener.uploadComplete(pd);

					fileProcessed(pd);

					// Update the database
					// _dbc.addImage(pd);
					break;
				case Constants.MSG_FILE_UPLOAD_FAILURE:
					Log.i(this.getClass().getName(), "MSG_FILE_UPLOAD_FAILURE");
					fileProcessed(null);
					// File upload failed. Do something here
					break;
				default:
					super.handleMessage(msg);
			}
			Log.i(this.getClass().getName(), "----- handleMessage -----");
		}

		// This method simply increments the processed files count and then checks to see if it matches the total number
		// that were to be processed. If it does, then a new event is fired off, letting the listeners know processing
		// has completed
		private void fileProcessed(PicData pd) {
			Log.i(this.getClass().getSimpleName(), "+++++ fileProcessed +++++");

			_processedFiles++;

			if (pd != null) {
				Log.i(this.getClass().getSimpleName(), "Adding item to uploadedlist");
				
				_uploadedList.add(pd);
			}

			if (_processedFiles == _totalNumFiles) {
				_s3UploadListener.uploadComplete(_uploadedList);
			}
			Log.i(this.getClass().getSimpleName(), "----- fileProcessed -----");
		}
	}

}
