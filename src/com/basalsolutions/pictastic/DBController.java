package com.basalsolutions.pictastic;

import java.util.ArrayList;
import java.util.List;

import com.google.common.io.Files;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.util.Log;

public class DBController extends SQLiteOpenHelper {

	private static final String _albumsTableName = "Albums";
	private static final String _imagesTableName = "Images";
	private static final String _dbName = "Pictastic";
	private static final int _dbVersion = 1;
	private MainListDataListener _dataListener;
	private ImageDataListener _imageDataListener;
	private SQLiteDatabase _database;

	public void set_dataListener(MainListDataListener dataListener) {
		this._dataListener = dataListener;
	}

	public DBController(Context context, String name, CursorFactory factory, int version) {
		super(context, name, factory, version);
	}

	@SuppressLint("NewApi")
	public DBController(Context context, String name, CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
		super(context, name, factory, version, errorHandler);
		// TODO Auto-generated constructor stub
	}

	public DBController(Context context) {
		super(context, _dbName, null, _dbVersion);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.i(this.getClass().getName(), "+++++ onCreate +++++");
		_database = db;
		createMainListItemsTable(db);
		createImagesTable(db);
	}

	private void createImagesTable(SQLiteDatabase db) {
		Log.i(this.getClass().getName(), "+++++ createImagesTable +++++");

		String createstr = "CREATE TABLE IF NOT EXISTS "
				+ _imagesTableName
				+ " (ID INTEGER PRIMARY KEY AUTOINCREMENT, IMAGELOCALPATH TEXT, POSTEDDATE TEXT, DATETAKEN TEXT, LIKES INTEGER, IMAGEMD5 TEXT, UPLOADED INTEGER, SYNCED INTEGER)";

		db.execSQL(createstr);

		if (tableExists(_albumsTableName, false)) {
			Log.i("DATABASECONTROLLER", "Images Table successfully created");
		} else {
			Log.i("DATABASECONTROLLER", "Images Table : FUCK YOU");
		}
	}

	private void createMainListItemsTable(SQLiteDatabase db) {
		String createstr = "CREATE TABLE IF NOT EXISTS " + _albumsTableName + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, ALBUMNAME TEXT)";

		db.execSQL(createstr);

		if (tableExists(_albumsTableName, false)) {
			Log.i("DATABASECONTROLLER", "Albums Table successfully created");
		} else {
			Log.i("DATABASECONTROLLER", "Albums Table : FUCK YOU");
		}
	}

	public boolean tableExists(String tableName, boolean openDb) {
		Log.i("DATABASECONTROLLER", "tableExists");

		Cursor cursor = _database.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + tableName + "'", null);

		if (cursor != null) {
			if (cursor.getCount() > 0) {
				cursor.close();
				return true;
			}
			cursor.close();
		}
		return false;
	}

	public void getAlbums() {
		// Call the async task

	}

	public void getImages() {
		// Call the async task to get all the images
		new AsyncGetImages().execute();
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		/*
		 * Log.i(this.getClass().getName(), "+++++ onUpgrade +++++");
		 * 
		 * String updatestr = "ALTER TABLE " + _imagesTableName + " ADD COLUMN UPLOADED INTEGER";
		 * 
		 * try { db.execSQL(updatestr); } catch (SQLException se) { se.printStackTrace(); }
		 * Log.i(this.getClass().getName(), "----- onUpgrade -----");
		 */
	}

	private void imageDataReceived(List<PicData> imagedata) {
		Log.i(this.getClass().getName(), "+++++ imageDataReceived +++++");
		_imageDataListener.onImageData(imagedata);
	}

	private void albumDataReceived(ArrayList<MainListData> albumdata) {
		_dataListener.onMainListDataListener(albumdata);
	}

	public void set_imageDataListener(ImageDataListener listener) {
		this._imageDataListener = listener;
	}

	public void addImage(PicData pd, int uploaded, int synced) {
		Log.i("DBController", "++++++++ addImages +++++++++");

		if (_database == null) {
			_database = getWritableDatabase();
		}

		ContentValues cv = new ContentValues();
		cv.put("IMAGELOCALPATH", pd.get_imageLocalPath());
		cv.put("UPLOADED", uploaded);
		cv.put("SYNCED", synced);
		cv.put("POSTEDDATE", pd.get_datePosted());
		cv.put("DATETAKEN", pd.get_dateTaken());
		cv.put("LIKES", pd.get_likes());

		try {
			long id = _database.insert(_imagesTableName, null, cv);
			Log.i("DBController", "id = " + id);
		} catch (SQLException se) {
			se.printStackTrace();
		}
	}

	public void updateUploaded(List<PicData> pdl) {
		for (PicData pd:pdl) {
			updateUploaded(pd);
		}
	}
	
	public void updateUploaded(PicData pd) {
		Log.i(this.getClass().getName(), "+++++ updateUploaded +++++");
		if (_database == null) {
			_database = getWritableDatabase();
		}

		String filter = "IMAGELOCALPATH='" +pd.get_imageLocalPath() +"'";
		ContentValues cv = new ContentValues();
		cv.put("UPLOADED", 1);
		
		int rows = _database.update(_imagesTableName, cv, filter, null);
		
		Log.i(this.getClass().getName(), "Rows updated = " + rows);
		
		Log.i(this.getClass().getName(), "----- updateUploaded -----");	
	}
	
	public long getImagesRecordsCount() {
		// TODO Auto-generated method stub
		SQLiteDatabase db = getReadableDatabase();
		return DatabaseUtils.queryNumEntries(db, _imagesTableName);
	}

	private class AsyncGetAlbums extends AsyncTask<SQLiteDatabase, Void, ArrayList<MainListData>> {

		private String[] _projection = { "ALBUMNAME" };

		@Override
		protected ArrayList<MainListData> doInBackground(SQLiteDatabase... params) {

			ArrayList<MainListData> albumlist = new ArrayList<MainListData>();

			SQLiteDatabase db = params[0];

			Cursor c = db.query(true, "ALBUMS", _projection, null, null, null, null, "ID", null);

			while (c.moveToNext()) {
				String name = c.getString(c.getColumnIndexOrThrow("ALBUMNAME"));

				MainListData mld = new MainListData();

				// Make sure something is there
				if (name.trim().length() > 0) {
					mld.set_text(name.trim());
				}
			}

			return albumlist;
		}

		@Override
		protected void onPostExecute(ArrayList<MainListData> result) {
			albumDataReceived(result);
		}

	}

	private class AsyncGetImages extends AsyncTask<Void, Void, ArrayList<PicData>> {

		@Override
		protected void onPostExecute(ArrayList<PicData> result) {
			imageDataReceived(result);
		}

		// TODO: Change this to return all the information about the image in a List<PicData>
		@Override
		protected ArrayList<PicData> doInBackground(Void... params) {
			Log.i(this.getClass().getName(), "+++++ doInBackground +++++");

			ArrayList<PicData> pdl = new ArrayList<PicData>();

			SQLiteDatabase db = getReadableDatabase();

			String[] projection = { "ID", "IMAGELOCALPATH", "POSTEDDATE", "DATETAKEN", "LIKES" };
			Cursor c = db.query(true, _imagesTableName, projection, null, null, null, null, "ID", null);

			Log.i(this.getClass().getName(), "Number of images returned: " + c.getCount());

			Log.i(this.getClass().getName(), "PicData Info");
			Log.i(this.getClass().getName(), "-------------");

			while (c.moveToNext()) {
				PicData pd = new PicData();

				pd.set_imageLocalPath(c.getString(c.getColumnIndexOrThrow("IMAGELOCALPATH")));
				pd.set_datePosted(c.getString(c.getColumnIndexOrThrow("POSTEDDATE")));
				pd.set_dateTaken(c.getString(c.getColumnIndexOrThrow("DATETAKEN")));
				pd.set_likes(c.getInt(c.getColumnIndexOrThrow("LIKES")));

				Log.i(this.getClass().getName(), "imageLocalPath : " + pd.get_imageLocalPath());
				Log.i(this.getClass().getName(), "datePosted : " + pd.get_datePosted());
				Log.i(this.getClass().getName(), "dateTaken : " + pd.get_dateTaken());
				Log.i(this.getClass().getName(), "likes : " + pd.get_likes());
				Log.i(this.getClass().getName(), "-------------");
				Log.i(this.getClass().getName(), "");

				pdl.add(pd);
			}

			return pdl;
		}

	}

}
