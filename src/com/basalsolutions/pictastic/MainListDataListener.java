package com.basalsolutions.pictastic;

import java.util.ArrayList;

public interface MainListDataListener {
	public abstract void onMainListDataListener(ArrayList<MainListData> albums);
}
