package com.basalsolutions.pictastic;

public class Constants {

	public static final int MSG_FILE_UPLOAD_SUCCESS = 1;
	public static final int MSG_FILE_UPLOAD_FAILURE = 2;
	public static final int MSG_REGISTER_CLIENT = 3;
	public static final int MSG_UPLOAD_FILE = 4;
	public static final int MSG_DOWNLOAD_FILE = 5;
	public static final int MSG_UNREGISTER_CLIENT = 6;
	public static final int MSG_FILE_UPLOAD_START = 7;
	public static final int MSG_UPLOADS_COMPLETE = 8;
	
	public Constants() {
		// TODO Auto-generated constructor stub
	}

}
