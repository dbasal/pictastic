package com.basalsolutions.pictastic;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;

public class LargeImageActivity extends Activity {
	String _filename;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_large_image);

		String filename;

		if (savedInstanceState == null) {
			Bundle extras = getIntent().getExtras();
			if (extras == null) {
				filename = null;
			} else {
				filename = extras.getString("filename");
			}
		} else {
			filename = (String) savedInstanceState.getSerializable("filename");
		}

		if (filename != null) {
			loadImage(filename);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.large_image, menu);
		return true;
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		String filename = savedInstanceState.getString("filename");
		loadImage(filename);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putString("filename", _filename);
		super.onSaveInstanceState(outState);
	}

	private void loadImage(String filename) {
		PictasticImageView piv = (PictasticImageView) findViewById(R.id.largeimage);

		// Load the file
		DisplayImageOptions dio = new DisplayImageOptions.Builder().cacheOnDisc(false).showImageForEmptyUri(R.drawable.noimage).build();

		// Now we have to load the image
		ImageLoader.getInstance().displayImage("file://" + filename, piv, dio);
		_filename = filename;
	}
}
