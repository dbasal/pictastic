package com.basalsolutions.pictastic;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.simpledb.AmazonSimpleDBClient;
import com.amazonaws.services.simpledb.model.BatchPutAttributesRequest;
import com.amazonaws.services.simpledb.model.ReplaceableAttribute;
import com.amazonaws.services.simpledb.model.ReplaceableItem;

import android.os.AsyncTask;
import android.util.Log;

public class SimpleDBLoader extends AsyncTask<List<PicData>, Void, List<PicData>> {

	private AmazonSimpleDBClient _sdbClient;
	private String _domain;
	private SimpleDBListener _simpleDBListener;

	public SimpleDBLoader(String domain, AmazonSimpleDBClient simpleDBClient) {
		_domain = domain;

		// Create the SimpleDB Client
		_sdbClient = simpleDBClient;
	}

	@Override
	protected List<PicData> doInBackground(List<PicData>... params) {

		ArrayList<PicData> pdl = (ArrayList<PicData>) params[0];

		ArrayList<ReplaceableItem> rilist = new ArrayList<ReplaceableItem>();

		for (PicData pd : pdl) {
			ReplaceableItem ri = createSimpleDBItem(pd, true);

			rilist.add(ri);
		}

		// Transmit the data to Amazon's SimpleDB
		sendDataToSimpleDB(rilist);

		return pdl;
	}

	private void sendDataToSimpleDB(ArrayList<ReplaceableItem> rilist) {
		Log.i(this.getClass().getSimpleName(), "+++++ sendDataToSimpleDB +++++");

		BatchPutAttributesRequest bpar = new BatchPutAttributesRequest(_domain, rilist);

		try {
			_sdbClient.batchPutAttributes(bpar);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onPostExecute(List<PicData> pdl) {
		Log.i(this.getClass().getSimpleName(), "+++++ onPostExecute +++++");
		_simpleDBListener.onUploadDataComplete(pdl);
	}

	/*
	 * SimpleDB structure:
	 * 
	 * Album User ImageName DatePosted DateTaken Likes
	 */
	private ReplaceableItem createSimpleDBItem(PicData pd, boolean replace) {
		Log.i(this.getClass().getSimpleName(), "ImageName = " + pd.get_imageName());
		Log.i(this.getClass().getSimpleName(), "UserName = " + pd.get_userName());
		Log.i(this.getClass().getSimpleName(), "DatePosted = " + pd.get_datePosted());
		Log.i(this.getClass().getSimpleName(), "DateTaken = " + pd.get_dateTaken());
		Log.i(this.getClass().getSimpleName(), "Likes = " + pd.get_likes());

		List<ReplaceableAttribute> attrs = new ArrayList<ReplaceableAttribute>(4);

		ReplaceableAttribute imagename = new ReplaceableAttribute("ImageName", pd.get_imageName(), replace);
		attrs.add(imagename);
		
		ReplaceableAttribute username = new ReplaceableAttribute("UserName", pd.get_userName(), replace);
		attrs.add(username);
		
		ReplaceableAttribute dateposted = new ReplaceableAttribute("DatePosted", pd.get_datePosted(), replace);
		attrs.add(dateposted);

		if (pd.get_dateTaken() != null) {
			ReplaceableAttribute datetaken = new ReplaceableAttribute("DateTaken", pd.get_dateTaken(), replace);
			attrs.add(datetaken);
		}
		
		ReplaceableAttribute likes = new ReplaceableAttribute("Likes", Integer.toString(pd.get_likes()), replace);

		attrs.add(likes);

		ReplaceableItem ri = new ReplaceableItem(pd.get_imageName() + pd.get_userName(), attrs);

		Log.i(this.getClass().getSimpleName(), "----- createSimpleDBItem -----");

		return ri;
	}

	public void set_simpleDBListener(SimpleDBListener _simpleDBListener) {
		this._simpleDBListener = _simpleDBListener;
	}

}
