package com.basalsolutions.pictastic;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

@SuppressLint("NewApi")
public class MainActivity extends Activity implements MainListDataListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.i(this.getClass().getName(), "+++++ onCreate +++++");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		setup();
		Log.i(this.getClass().getName(), "----- onCreate -----");
	}

	private void setup() {
		Log.i(this.getClass().getName(), "+++++ setup +++++");
		ActionBar actionbar = getActionBar();
		actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		actionbar.setDisplayShowTitleEnabled(false);

		AlbumFragment afrag = new AlbumFragment();
		afrag.setText("Test this motherfucker");

		AlbumFragment afrag2 = new AlbumFragment();
		afrag2.setText("Fuck You");
		actionbar.addTab(actionbar.newTab().setText("Tab 1").setTabListener(new TabListener(afrag)), true);
		actionbar.addTab(actionbar.newTab().setText("Tab 2").setTabListener(new TabListener(afrag2)), false);

		// actionbar.new
		/*
		 * TODO: Uncomment after testing the actionbar DBController dbcontroller = new DBController(this);
		 * dbcontroller.set_dataListener(_listDataListener); dbcontroller.set_dataListener(this);
		 * dbcontroller.getAlbums();
		 */
		Log.i(this.getClass().getName(), "----- setup -----");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.actionbarmenu, menu);
		return true;
	}

	@Override
	public void onMainListDataListener(ArrayList<MainListData> albums) {
		// Make sure we have something to display
		if (albums.size() > 0) {
			// Add these items to the action bar tabs
		}
	}

	private class TabListener implements ActionBar.TabListener {
		private AlbumFragment _fragment;

		public TabListener(AlbumFragment albumfrag) {
			_fragment = albumfrag;
		}

		@Override
		public void onTabReselected(Tab tab, FragmentTransaction ft) {
		}

		@Override
		public void onTabSelected(Tab tab, android.app.FragmentTransaction ft) {
			ft.add(R.id.fragment_content, _fragment, _fragment.getText());
		}

		@Override
		public void onTabUnselected(Tab tab, android.app.FragmentTransaction ft) {
			ft.remove(_fragment);
		}
	}

	public static class AlbumFragment extends Fragment implements ImageDataListener {
		private String _text;
		private PictasticGridView _gridView;

		public AlbumFragment() {
		}

		public void setText(String text) {
			_text = text;
		}

		public String getText() {
			return _text;
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			Log.i(this.getClass().getName(), "+++++ onCreateView +++++");

			View frag = inflater.inflate(R.layout.album_fragment, container, false);

			_gridView = (PictasticGridView) frag.findViewById(R.id.gvAlbumImages);

			int imagewidth = _gridView.getWidth();
			int imageheight = _gridView.getHeight();
			
			Log.i(this.getClass().getSimpleName(), "width = " + imagewidth);
			Log.i(this.getClass().getSimpleName(), "height = " + imageheight);
			
			DBController dbcontroller = new DBController(getActivity());

			dbcontroller.set_imageDataListener(this);

			long cnt = dbcontroller.getImagesRecordsCount();

			Log.i(this.getClass().getName(), "Number of images in DB : " + cnt);

			// Need to get the files from the database
			dbcontroller.getImages();
			
			Log.i(this.getClass().getName(), "----- onCreateView -----");

			return frag;
		}

		@Override
		public void onImageData(List<PicData> pdl) {
			// Once we have the filenames back, we can send them to the adapter
			Log.i(this.getClass().getName(), "+++++ onFileNames +++++");
			AlbumAdapter adapter = new AlbumAdapter(getActivity(), R.layout.album_image, pdl);
			_gridView.setAdapter(adapter);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_upload:
				uploadToAWS();
				return true;
			default:
				return super.onOptionsItemSelected(item);

		}
	}

	private void uploadToAWS() {
		
	}

}
