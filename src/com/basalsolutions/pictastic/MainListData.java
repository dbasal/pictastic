package com.basalsolutions.pictastic;

import android.widget.TextView;

public class MainListData {

	private String _text;
	private int _totalItems;
	private int _newItems;
	
	public String get_text() {
		return _text;
	}

	public void set_text(String _text) {
		this._text = _text;
	}

	public int get_totalItems() {
		return _totalItems;
	}

	public void set_totalItems(int _totalItems) {
		this._totalItems = _totalItems;
	}

	public int get_newItems() {
		return _newItems;
	}

	public void set_newItems(int _newItems) {
		this._newItems = _newItems;
	}

	public MainListData() {
		// TODO Auto-generated constructor stub
	}
	
}


