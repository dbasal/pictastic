package com.basalsolutions.pictastic;

import java.util.ArrayList;
import java.util.List;

public interface CRListener {
	public abstract void onFilenames(List<PicData> pdl);
}
