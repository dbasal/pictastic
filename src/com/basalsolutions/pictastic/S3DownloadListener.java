package com.basalsolutions.pictastic;

import java.util.ArrayList;

public interface S3DownloadListener {
	public abstract void onDownloadComplete(ArrayList<String> list);

}
