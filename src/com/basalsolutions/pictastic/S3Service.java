package com.basalsolutions.pictastic;

import java.util.ArrayList;
import java.util.Random;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.google.common.io.Files;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class S3Service extends Service {

	private ArrayList<Messenger> _clients = new ArrayList<Messenger>();
	final Messenger _messenger = new Messenger(new IncomingHandler());

	public S3Service() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public IBinder onBind(Intent intent) {
		Log.i(this.getClass().getName(), "onBind");		
		return _messenger.getBinder();
	}

	private void createMessageWithObject(int msgtype, Object obj) {
		Message msg = Message.obtain();
		msg.what = msgtype;
		msg.obj = obj;

		for (int i = 0; i < _clients.size(); i++) {
			try {
				_clients.get(i).send(msg);

			} catch (RemoteException re) {
				_clients.remove(i);
			}
		}
	}

	private void createMessage(int msgtype) {
		for (int i = 0; i < _clients.size(); i++) {
			try {
				Message msg = Message.obtain();
				msg.what = msgtype;
				_clients.get(i).send(msg);

			} catch (RemoteException re) {
				_clients.remove(i);
			}
		}
	}

	class IncomingHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case Constants.MSG_REGISTER_CLIENT:
					Log.i("IncomingHandler", "MSG_REGISTER_CLIENT");
					_clients.add(msg.replyTo);
					break;
				case Constants.MSG_UPLOAD_FILE:
					Log.i("IncomingHandler", "MSG_UPLOAD_FILE");

					// Message received to upload file. Get the file info from the obj and call uploadFile
					uploadFile(msg.obj);
					break;
				case Constants.MSG_DOWNLOAD_FILE:
					Log.i("IncomingHandler", "MSG_COLLECTING_DATA_RESUME");
					break;
				default:
					super.handleMessage(msg);
					break;
			}
		}

		private void uploadFile(Object obj) {
			if (obj.getClass().equals(S3Info.class)) {
				S3Info s3info = (S3Info) obj;

				final String filename = s3info.get_filename();

				final AmazonS3Client s3Client = s3info.get_s3Client();

				final String bucket = s3info.get_bucket();

				String key = Files.getNameWithoutExtension(filename);

				final PutObjectRequest por = new PutObjectRequest(bucket, key, new java.io.File(filename));

				new Thread(new Runnable() {

					@Override
					public void run() {
						try {
							if (!s3Client.doesBucketExist(bucket)) {
								s3Client.createBucket(bucket);
							}
							
							// Create the message to tell the controller the upload has started
							createMessageWithObject(Constants.MSG_FILE_UPLOAD_START, filename);
							PutObjectResult res = s3Client.putObject(por);

							// Send message to let the controller know the upload is complete
							createMessageWithObject(Constants.MSG_FILE_UPLOAD_SUCCESS, filename);

							// Log.i("PutObjectResult", "Key : " + key + " - " + res.toString());
						} catch (Exception e) {
							e.printStackTrace();
						}

					}
				}).start();

			}
		}
	}
}
