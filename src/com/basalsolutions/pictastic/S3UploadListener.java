package com.basalsolutions.pictastic;

import java.util.ArrayList;
import java.util.List;

public interface S3UploadListener {
	public abstract void uploadComplete(PicData picdata);
	public abstract void uploadComplete(List<PicData> pdl);
}
