package com.basalsolutions.pictastic;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;

public class CRAsync extends AsyncTask<List<PicData>, Void, List<PicData>> {

	private CRListener _crListener;
	private ContentResolver _resolver;
	private Context _context;
	private static final String[] _projection = {MediaStore.MediaColumns.DATA};

	public void set_crListener(CRListener listener) {
		_crListener = listener;
	}
	
	public CRAsync(Context context) {
		_context = context;
//		_resolver = _context.getContentResolver();
	}
	
	private String getFullPath(String uri) {
		String filename = null;
	
        Cursor metaCursor = _context.getContentResolver().query(Uri.parse(uri),_projection, null, null, null);
        
        if (metaCursor != null) {
            try {
                if (metaCursor.moveToFirst()) {
                    filename = metaCursor.getString(0);
                }
            } finally {
                metaCursor.close();
            }
        }

    return filename;	
	}
	
	@Override
	protected List<PicData> doInBackground(List<PicData>... sList) {
		List<PicData> list = sList[0];
//		List<PicData> pdl = new ArrayList<PicData>();
	
		for (PicData pd:list) {
			String fname = getFullPath(pd.get_contentUri());
			
			pd.set_imageLocalPath(fname);
		}
		
		return list;
	}

	@Override
	protected void onPostExecute(List<PicData> result) {
		_crListener.onFilenames(result);
	}

}
