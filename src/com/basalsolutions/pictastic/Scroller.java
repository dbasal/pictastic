package com.basalsolutions.pictastic;

import android.view.View;
import android.widget.ScrollView;

public class Scroller implements Runnable {

	ScrollView _scroller;
	View       _child;

	public Scroller(ScrollView sview, View child) {
		_scroller = sview;
		_child = child;
	}

	@Override
	public void run() {
		_scroller.scrollTo(0, _child.getTop());		
	}

}
