package com.basalsolutions.pictastic;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.google.common.io.Files;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class S3Uploader extends AsyncTask<List<PicData>, String, List<PicData>> {

	private String _bucketName;
	private AmazonS3Client _s3Client;
	private S3UploadListener _s3UploadListener;
	private ProgressDialog _progressDialog;
	private Context _context;

	public void set_s3UploadListener(S3UploadListener _s3UploadListener) {
		this._s3UploadListener = _s3UploadListener;
	}

	public S3Uploader(String bucketname, AmazonS3Client s3client, S3UploadListener listener, Context context) {
		_bucketName = bucketname;
		_s3Client = s3client;
		_s3UploadListener = listener;
		_context = context;
	}

	@Override
	protected List<PicData> doInBackground(List<PicData>... params) {
		List<PicData> outlist = new ArrayList<PicData>();

		if (!_s3Client.doesBucketExist(_bucketName)) {
			_s3Client.createBucket(_bucketName);
		}

		List<PicData> pdlist = params[0];

		// Loop through each file and send them to S3
		int i = 0;

		for (PicData pd : pdlist) {
			i++;
			publishProgress("Uploading " + i + " of " + pdlist.size() + ": " + Files.getNameWithoutExtension(pd.get_imageLocalPath()));
			
			Log.i(this.getClass().getName(), "Uploading: " + Files.getNameWithoutExtension(pd.get_imageLocalPath()));
			String key = Files.getNameWithoutExtension(pd.get_imageLocalPath());
			PutObjectRequest por = new PutObjectRequest(_bucketName, key, new java.io.File(pd.get_imageLocalPath()));
			outlist.add(pd);
			
			try {
				PutObjectResult res = _s3Client.putObject(por);
				Log.i("PutObjectResult", "Key : " + key + " - " + res.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return outlist;
	}

	@Override
	protected void onPreExecute() {
		_progressDialog = new ProgressDialog(_context);
		_progressDialog.setMessage("Preparing to upload ...");
		_progressDialog.setCancelable(false);
		_progressDialog.show();
	}

	@Override
	protected void onPostExecute(List<PicData> result) {
		_progressDialog.dismiss();

		//_s3UploadListener.uploadComplete(result);
	}

	@Override
	protected void onProgressUpdate(String... values) {
		String str = values[0];
		_progressDialog.setMessage(str);

	}

}
