package com.basalsolutions.pictastic;

import java.util.List;
import java.util.Random;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.google.common.io.Files;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class AWSController implements S3UploadListener, SimpleDBListener {

	private Context _context;
	private S3Controller _s3Controller;
	private SimpleDBController _simpleDBController;
	private S3UploadListener _s3UploadListener;
	private SimpleDBListener _simpleDBListener;
	
	AWSCredentials _creds = new BasicAWSCredentials("AKIAIQWEAMTR4YPTBRCQ", "Sk7lW57bpVa5hwKjrps5bCoptgyU1SoHhDEcARxa");

	public AWSController(Context context) {
		_context = context;
		_s3Controller = new S3Controller(_context, _creds);
		_s3Controller.set_s3UploadListener(this);
	}

	public void uploadToS3(List<PicData> pdl) {
		_s3Controller.uploadFiles(pdl);
	}

	public void uploadToSimpleDB(List<PicData> pdl) {
		_simpleDBController = new SimpleDBController(_context, _creds);
		_simpleDBController.set_simpleDBListener(this);
		_simpleDBController.addImageInfo(pdl);
	}

	public void set_s3UploadListener(S3UploadListener _s3UploadListener) {
		this._s3UploadListener = _s3UploadListener;
	}

	@Override
	public void uploadComplete(PicData picdata) {
		_s3UploadListener.uploadComplete(picdata);
	}

	@Override
	public void uploadComplete(List<PicData> pdl) {
		this._s3UploadListener.uploadComplete(pdl);
	}

	@Override
	public void onUploadDataComplete(List<PicData> list) {
		// Let the listeners know
		_simpleDBListener.onUploadDataComplete(list);
	}

	public void set_simpleDBListener(SimpleDBListener _simpleDBListener) {
		this._simpleDBListener = _simpleDBListener;
	}
}