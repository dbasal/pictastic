package com.basalsolutions.pictastic;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.common.io.Files;

import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView.ScaleType;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;

public class ShareActivity extends Activity {

	private TableLayout _tableLayout;

	private static int _imagesPerRow = 4;
	private int _imagewidth;
	private int _imageheight;
	private static final int SELECT_IMAGE = 10;
	private ScrollView _scrollView;
	private Button _btnSubmit;
	private ShareController _shareController;
	private static final String[] _projection = { MediaStore.MediaColumns.DATA };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_share);
		_shareController = new ShareController(this);
		// _s3Controller = new S3Controller(this,new BasicAWSCredentials("AKIAIQWEAMTR4YPTBRCQ",
		// "Sk7lW57bpVa5hwKjrps5bCoptgyU1SoHhDEcARxa"));

		_scrollView = (ScrollView) findViewById(R.id.scroller);

		// Setup the button for allowing submissions of the images
		_btnSubmit = (Button) findViewById(R.id.btnSubmit);

		// ----------- Button onClickListener
		_btnSubmit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				processTableLayout();

				// Need to fire off the main activity again
				// launchMainActivity();
				// finish();
			}

		});
		// --------- End Button onClickListener

		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		int height = displaymetrics.heightPixels;
		int width = displaymetrics.widthPixels;

		// Determine the width of each image. The height will be the same
		if (height > width) {
			_imagewidth = width / _imagesPerRow;
		} else {
			_imagewidth = height / _imagesPerRow;
		}

		_imageheight = _imagewidth;

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		String action = intent.getAction();

		_tableLayout = (TableLayout) findViewById(R.id.tlMain);

		// if this is from the share menu
		if (Intent.ACTION_SEND.equals(action)) {
			if (extras.containsKey(Intent.EXTRA_STREAM)) {
				try {
					// Get resource path from intent called
					Uri uri = (Uri) extras.getParcelable(Intent.EXTRA_STREAM);
					loadImage(uri);

					// Query gallery for camera picture via
					// Android ContentResolver interface
					// ContentResolver cr = getContentResolver();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	/*
	 * ShareActivity::processTableLayout: This method loops through all the rows in all the tablelayouts and get the uri
	 * of the image and stores it in an arraylist, which is returned
	 */

	private String getDateTaken(String imagepath) {
		ExifInterface intf = null;
		try {
			intf = new ExifInterface(imagepath);
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (intf == null) {
			/* File doesn't exist or isn't an image */
		}

		String dateString = intf.getAttribute(ExifInterface.TAG_DATETIME);

		return dateString;
	}

	private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			// Calculate ratios of height and width to requested height and width
			final int heightRatio = Math.round((float) height / (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will guarantee
			// a final image with both dimensions larger than or equal to the
			// requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}

		return inSampleSize;
	}

	private static Bitmap decodeSampledBitmapFromFile(String filename, int reqWidth, int reqHeight) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(filename, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeFile(filename, options);
	}

	private void processTableLayout() {
		ArrayList<Uri> urilist = new ArrayList<Uri>();

		List<PicData> pdl = new ArrayList<PicData>();

		// Loop through the tablelayout in order to get each tablerow and then further process that
		for (int i = 0; i <= _tableLayout.getChildCount() - 1; i++) {
			TableRow tr = (TableRow) _tableLayout.getChildAt(i);

			// The tablerow consists of one to many PictasticImageViews
			for (int j = 0; j <= tr.getChildCount() - 1; j++) {
				PictasticImageView piv = (PictasticImageView) tr.getChildAt(j);

				// Make sure we are not dealing with the plussign
				if (!piv.is_plussign()) {
					// Add the current filename to the list. This is actually a Uri content:// string
					pdl.add(piv.get_metadata());

					urilist.add(piv.get_contentUri());
				}
			}
		}
		// _s3Controller.uploadFiles(pdl);
		_shareController.shareFiles(pdl);

		finish();
	}

	private PictasticImageView createImageView(Uri uri) {
		PicData pd = new PicData();
		pd.set_contentUri(uri.toString());
		pd.set_imageLocalPath(getFullPath(uri.toString()));
		pd.set_imageName(Files.getNameWithoutExtension(pd.get_imageLocalPath()));
		pd.set_datePosted(new Date().toString());
		pd.set_dateTaken(getDateTaken(pd.get_imageLocalPath()));

		pd.set_userName("MrTest");

		Log.i(this.getClass().getName(), "imageLocalPath = " + pd.get_imageLocalPath());

		PictasticImageView iv = new PictasticImageView(this, pd);

		iv.setBackgroundColor(Color.LTGRAY);
		iv.setPadding(1, 1, 1, 1);

		TableRow tr = null;

		// Check to see if we need to add a tablerow
		if (_tableLayout.getChildCount() == 0) {
			tr = new TableRow(this);

			// Add the imageview to the tablerow
			tr.addView(iv);

			// Make sure to add it to the tablelayout once it has been created
			_tableLayout.addView(tr);
		} else {
			// Get the current tablerow out of the tablelayout
			tr = (TableRow) _tableLayout.getChildAt(_tableLayout.getChildCount() - 1);

			// Delete the + sign, which will always be the last image under this condition
			tr.removeViewAt(tr.getChildCount() - 1);

			tr.addView(iv);
		}

		// Check the table row again to determine where to place the + sign and if we need to add another row to the
		// layout
		if (tr.getChildCount() == _imagesPerRow) {
			tr = new TableRow(this);
			_tableLayout.addView(tr);
			createPlusSign(tr);

			// Need to make sure the most recent table row is shown
			_scrollView.post(new Scroller(_scrollView, tr));
		} else {
			createPlusSign(tr);
		}

		return iv;
	}

	private void createPlusSign(TableRow parent) {
		PicData pd = new PicData();
		pd.set_plussign(true);

		PictasticImageView plussign = new PictasticImageView(this, pd);

		plussign.setImageResource(R.drawable.newpicture);

		plussign.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// Open the Gallery and allow the user to select an image
				Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				intent.setType("image/*");
				startActivityForResult(intent, SELECT_IMAGE);
			}
		});

		parent.addView(plussign);
		TableRow.LayoutParams llparams = new TableRow.LayoutParams();
		llparams.gravity = Gravity.CENTER;
		llparams.height = _imageheight;
		llparams.width = _imagewidth;

		plussign.setLayoutParams(llparams);
	}

	private void loadImage(Uri uri) {
		// Need to determine if we need to create a new row in the table
		PictasticImageView imageview = createImageView(uri);
		imageview.getLayoutParams().height = _imageheight;
		imageview.getLayoutParams().width = _imagewidth;
		imageview.setScaleType(ScaleType.CENTER_CROP);
		imageview.set_contentUri(uri);
		imageview.setImageBitmap(decodeSampledBitmapFromFile(imageview.get_filename(), _imageheight, _imagewidth));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.share, menu);
		return true;
	}

	private String getFullPath(String uri) {
		String filename = null;

		Cursor metaCursor = this.getContentResolver().query(Uri.parse(uri), _projection, null, null, null);

		if (metaCursor != null) {
			try {
				if (metaCursor.moveToFirst()) {
					filename = metaCursor.getString(0);
				}
			} finally {
				metaCursor.close();
			}
		}

		return filename;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
		super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
		if (resultCode != RESULT_CANCELED) {
			switch (requestCode) {
				case SELECT_IMAGE:
					if (resultCode == RESULT_OK) {
						Uri selectedImage = imageReturnedIntent.getData();

						// String filename = getRealPathFromURI(selectedImage);

						// Get a new ImageView
						PictasticImageView imageview = createImageView(selectedImage);
						imageview.set_contentUri(selectedImage);
						imageview.getLayoutParams().height = _imageheight;
						imageview.getLayoutParams().width = _imagewidth;
						imageview.setScaleType(ScaleType.CENTER_CROP);

						imageview.setImageBitmap(decodeSampledBitmapFromFile(imageview.get_filename(), _imageheight, _imagewidth));
						// ImageLoader.getInstance().displayImage(selectedImage.toString(), imageview);
					}
			}
		}
	}
}
