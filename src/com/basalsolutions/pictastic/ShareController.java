package com.basalsolutions.pictastic;

import java.util.List;

import com.amazonaws.auth.BasicAWSCredentials;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class ShareController implements S3UploadListener, SimpleDBListener {

	private Context _context;
	private AWSController _awsController;
	private DBController _dbController;

	// private S3Controller _s3Controller;

	public ShareController(Context context) {
		System.setProperty("org.xml.sax.driver", "org.xmlpull.v1.sax2.Driver");
		_context = context;
		_dbController = new DBController(_context);
		_awsController = new AWSController(_context);
		_awsController.set_s3UploadListener(this);
		_awsController.set_simpleDBListener(this);
		
		// _s3Controller = new S3Controller(_context, new BasicAWSCredentials("AKIAIQWEAMTR4YPTBRCQ",
		// "Sk7lW57bpVa5hwKjrps5bCoptgyU1SoHhDEcARxa"));
	}

	public void shareFiles(List<PicData> pdl) {
		ConnectivityManager connManager = (ConnectivityManager) _context.getSystemService(_context.CONNECTIVITY_SERVICE);
		NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

		if (mWifi.isConnected()) {
			_awsController.uploadToS3(pdl);
		} else {
			// If the wifi is not connected, then we must upload the information to the database for syncing later
		}
	}

	@Override
	public void uploadComplete(PicData picdata) {
		Log.i(this.getClass().getSimpleName(), "+++++ updateComplete individual item +++++");
		// Update the local database
		_dbController.addImage(picdata, 1, 0);
	}

	@Override
	public void uploadComplete(List<PicData> pdl) {
		Log.i(this.getClass().getSimpleName(), "+++++ updateComplete list +++++");
		// This tell us that all the files have been uploaded to the cloud and the list are the ones that were
		// successful. We can update the SimpleDB
		_awsController.uploadToSimpleDB(pdl);
	}

	@Override
	public void onUploadDataComplete(List<PicData> list) {
		// Once we know the information has been successfully stored in the SimpleDB server, we can set the sync flag to
		// true for the items uploaded
		_dbController.updateUploaded(list);
	}
}
