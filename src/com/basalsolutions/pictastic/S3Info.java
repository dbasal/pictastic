package com.basalsolutions.pictastic;

import com.amazonaws.services.s3.AmazonS3Client;

public class S3Info {

	private String _bucket;
	private AmazonS3Client _s3Client;
	private String _filename;
	
	public S3Info() {
		// TODO Auto-generated constructor stub
	}

	public String get_bucket() {
		return _bucket;
	}

	public void set_bucket(String bucket) {
		this._bucket = bucket;
	}

	public AmazonS3Client get_s3Client() {
		return _s3Client;
	}

	public void set_s3Client(AmazonS3Client s3Client) {
		this._s3Client = s3Client;
	}

	public String get_filename() {
		return _filename;
	}

	public void set_filename(String filename) {
		this._filename = filename;
	}

}
