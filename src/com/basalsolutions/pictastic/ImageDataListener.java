package com.basalsolutions.pictastic;

import java.util.ArrayList;
import java.util.List;

public interface ImageDataListener {
	public abstract void onImageData(List<PicData> imagedata);
}
