package com.basalsolutions.pictastic;

import android.content.Context;
import android.net.Uri;
import android.util.AttributeSet;
import android.widget.ImageView;

public class PictasticImageView extends ImageView {

	private Uri _contentUri;
	private String _filename;
	private boolean _plussign;
	private PicData _metadata;

	public boolean is_plussign() {
		return _metadata.is_plussign();
	}

	public void set_plussign(boolean plussign) {
		_metadata.set_plussign(plussign);
	}

	public String get_filename() {
		return _metadata.get_imageLocalPath();
	}

	public void set_filename(String filename) {
		_metadata.set_imageLocalPath(filename);
	}

	public PictasticImageView(Context context, PicData metadata) {
		super(context);
		_metadata = metadata;
	}

	public PictasticImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public PictasticImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}

	public Uri get_contentUri() {
		return _contentUri;
	}

	public void set_contentUri(Uri _contentUri) {
		this._contentUri = _contentUri;
	}

	public PicData get_metadata() {
		return _metadata;
	}

	public void set_metadata(PicData _metadata) {
		this._metadata = _metadata;
	}

}
