package com.basalsolutions.pictastic;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.services.simpledb.AmazonSimpleDBClient;
import com.amazonaws.services.simpledb.model.CreateDomainRequest;
import com.amazonaws.services.simpledb.model.ListDomainsRequest;

public class SimpleDBController implements SimpleDBListener {

	private static final String _domain = "PicImages";
	private AmazonSimpleDBClient _sdbClient;
	private SimpleDBListener _simpleDBListener;
	private SimpleDBLoader _simpleDBLoader;

	public SimpleDBController(Context context, AWSCredentials creds) {
		_sdbClient = new AmazonSimpleDBClient(creds);

		/*
		 * if (createDomain(_domain)) { Log.i(this.getClass().getName(), "Domain Ready"); }
		 */
	}

	private boolean createDomain(String domain) {
		Log.i(this.getClass().getName(), "+++++ createDomain +++++");

		// Create the domain in the simpledb
		CreateDomainThread cdt = new CreateDomainThread();

		try {
			Log.i(this.getClass().getName(), "joining thread");
			cdt.join();
		} catch (InterruptedException e) {

		}

		Log.i(this.getClass().getName(), "----- createDomain -----");

		return true;
	}

	public void addImageInfo(List<PicData> pdl) {
		Log.i(this.getClass().getSimpleName(), "+++++ addImageInfo +++++");
		_simpleDBLoader = new SimpleDBLoader(_domain, _sdbClient);
		_simpleDBLoader.set_simpleDBListener(this);
		_simpleDBLoader.execute(pdl);
	}

	private class CreateDomainThread extends Thread {

		@Override
		public void run() {
			super.run();
			CreateDomainRequest cdr = new CreateDomainRequest(_domain);
			_sdbClient.createDomain(cdr);
		}

	}

	@Override
	public void onUploadDataComplete(List<PicData> list) {
		// Continue to pass it up the stack
		Log.i(this.getClass().getSimpleName(),"+++++ onUploadDataComplete +++++");
		
		_simpleDBListener.onUploadDataComplete(list);
	}

	public void set_simpleDBListener(SimpleDBListener _simpleDBListener) {
		this._simpleDBListener = _simpleDBListener;
	}

}
