package com.basalsolutions.pictastic;

public class PicData {

	private String _imageName;
	private String _userName;
	private String _datePosted;
	private String _dateTaken;
	private int _likes;
	private String _imageLocalPath;
	private String _contentUri;
	private boolean _plussign;
	private boolean _uploaded;
	private String _imageMD5;	
	
	public String get_datePosted() {
		return _datePosted;
	}

	public void set_datePosted(String dateposted) {
		this._datePosted = dateposted;
	}

	public int get_likes() {
		return _likes;
	}

	public void set_likes(int likes) {
		this._likes = likes;
	}

	public String get_imageName() {
		return _imageName;
	}

	public void set_imageName(String imagename) {
		this._imageName = imagename;
	}

	public String get_userName() {
		return _userName;
	}

	public void set_userName(String username) {
		this._userName = username;
	}

	public PicData() {
		_likes = 0;
	}

	public String get_imageLocalPath() {
		return _imageLocalPath;
	}

	public void set_imageLocalPath(String imagelocalpath) {
		this._imageLocalPath = imagelocalpath;
	}

	public String get_contentUri() {
		return _contentUri;
	}

	public void set_contentUri(String _contentUri) {
		this._contentUri = _contentUri;
	}

	public boolean is_plussign() {
		return _plussign;
	}

	public void set_plussign(boolean _plussign) {
		this._plussign = _plussign;
	}

	public String get_dateTaken() {
		return _dateTaken;
	}

	public void set_dateTaken(String _dateTaken) {
		this._dateTaken = _dateTaken;
	}

	public boolean is_uploaded() {
		return _uploaded;
	}

	public void set_uploaded(boolean _uploaded) {
		this._uploaded = _uploaded;
	}

}
