package com.basalsolutions.pictastic;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MainListAdapter extends ArrayAdapter<MainListData> {

	private int _layoutResourceID;
	private LayoutInflater _inflater;
	private List<MainListData> _mainListData;
	
	public MainListAdapter(Context context, int textViewResourceId) {
		super(context, textViewResourceId);
		// TODO Auto-generated constructor stub
	}

	public MainListAdapter(Context context, int resource, int textViewResourceId) {
		super(context, resource, textViewResourceId);
		// TODO Auto-generated constructor stub
	}

	public MainListAdapter(Context context, int textViewResourceId, MainListData[] objects) {
		super(context, textViewResourceId, objects);
		// TODO Auto-generated constructor stub
	}

	public MainListAdapter(Context context, int textViewResourceId, List<MainListData> objects) {
		super(context, textViewResourceId, objects);
		// TODO Auto-generated constructor stub
	}

	public MainListAdapter(Context context, int resource, int textViewResourceId, MainListData[] objects) {
		super(context, resource, textViewResourceId, objects);
		// TODO Auto-generated constructor stub
	}

	public MainListAdapter(Context context, int resource, int textViewResourceId, List<MainListData> objects) {
		super(context, resource, textViewResourceId, objects);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		DataHolder holder = null;

		if (convertView == null) {
			holder = new DataHolder();
			convertView = _inflater.inflate(_layoutResourceID, null);

			// Set up the holder with all the fields that are required to display
			holder._text = (TextView) convertView.findViewById(R.id.tvMain);
			convertView.setTag(holder);
		} else {
			holder = (DataHolder) convertView.getTag();
		}
		
		MainListData mld = _mainListData.get(position);
		holder._text.setText(mld.get_text());
		
		return convertView;
	}

	static class DataHolder {
		TextView _text;
		TextView _count;
		TextView _new;
	}
}
