package com.basalsolutions.pictastic;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.entity.FileEntity;

import com.google.common.base.Strings;
import com.google.common.io.Files;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.utils.StorageUtils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

public class AlbumAdapter extends ArrayAdapter<PicData> {

	private static final int _imagesPerRow = 3;
	private int _imageheight;
	private int _imagewidth;
	private Context _context;
	private List<PicData> _pdList;

	/*
	 * public AlbumAdapter(Context context, int layout, PicData[] pdl) { super(context, layout, pdl); }
	 */
	public AlbumAdapter(Context context, int layout, List<PicData> pdlist) {
		super(context, layout, pdlist);
		_context = context;
		_pdList = pdlist;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Log.i(this.getClass().getName(), "position = " + position);

		ImageHolder holder = null;
		PictasticImageView piv = null;
		PicData pd = _pdList.get(position);
		Log.i(this.getClass().getName(), "imageLocalPath : " + pd.get_imageLocalPath());

		// Calculate the size the image needs to be
		_imagewidth = parent.getWidth() / _imagesPerRow;
		_imageheight = _imagewidth;

		if (convertView == null) {
			Log.i(this.getClass().getName(), "convertView is null");
			holder = new ImageHolder();

			piv = new PictasticImageView(_context, pd);

			piv.setLayoutParams(new GridView.LayoutParams(_imagewidth, _imageheight));

			piv.setScaleType(ScaleType.CENTER_CROP);
			piv.setBackgroundColor(Color.LTGRAY);
			piv.set_metadata(pd);

			piv.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					PictasticImageView picview = (PictasticImageView) v;

					Class dvClass = null;
					Intent dvIntent = null;

					try {
						dvClass = Class.forName("com.basalsolutions.pictastic.LargeImageActivity");
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					dvIntent = new Intent(_context, dvClass);
					dvIntent.putExtra("filename", picview.get_filename());
					_context.startActivity(dvIntent);

					Log.i(this.getClass().getSimpleName(), "Image clicked : " + picview.get_filename());

				}
			});

			holder._image = piv;

			convertView = piv;
			convertView.setTag(holder);
		} else {
			Log.i(this.getClass().getName(), "convertView NOT null");
			holder = (ImageHolder) convertView.getTag();
		}

		String filename = pd.get_imageLocalPath();

		Log.i(this.getClass().getSimpleName(), "_imagewidth = " + _imagewidth + " _imageheight = " + _imageheight);

		DisplayImageOptions dio = new DisplayImageOptions.Builder().cacheOnDisc(true).showImageForEmptyUri(R.drawable.noimage).build();

		// Now we have to load the image
		ImageLoader.getInstance().displayImage("file://" + filename, holder._image, dio);
		return convertView;
	}

	static class ImageHolder {
		PictasticImageView _image;
	}

}
